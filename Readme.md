---
title: Spike Report
---

Core 5 – UMG UI Designer
========================

Introduction
------------

We’ve come a long way already, with a little game prototype that handles
most of the Unreal Gameplay Framework, and now we want to put in some
polish into our game.

Any gameplay-complete prototype needs a few things to make the User
eXperience complete – and we’re going to start by learning how to make a
HUD (Heads Up Display) and a Menu.

Goals
-----

Building on Core Spike 4 (Unreal Engine Gameplay Framework), add:

1.  A Heads-Up Display which displays some values of the Pawn which we
    are possessing. You can do this in one of several ways:

<!-- -->

1.  **\[Preferred\] In your custom PlayerController, create your Widget
    and attach it**

2.  \[Rudimentary\] In the Level Blueprint, create/attach the Widget to
    Player Controller 0

3.  \[Depreciated\] Add a custom HUD class to your custom GameMode

<!-- -->

1.  A Main Menu, in a separate scene, which has 3 buttons:

    a.  Start a New Game

    b.  Show some Info (Gameplay controls)

    c.  Or Quit the game

2.  A Pause Menu, during which:

    a.  The game pauses, and the mouse is shown (if hidden)

    b.  The player can Return to the Game (re-hides the mouse)

    c.  The player can Go to the Menu (change scene)

Personnel
---------

In this section, list the primary author of the spike report, as well as
any personnel who assisted in completing the work.

  ------------------- -----------------
  Primary – Jared K   Secondary – N/A
  ------------------- -----------------

Technologies, Tools, and Resources used
---------------------------------------

-   Unreal Engine VR Documentation:
    <https://docs.unrealengine.com/latest/INT/Platforms/VR/>

-   Main Menu Tutorial <https://www.youtube.com/watch?v=ulUO4EN8BG8>

Tasks undertaken
----------------

**Main Menu:**

Files required:

-   1 Blank level (Menu)

-   2 UI files (Main Menu, Controls)

The Main Menu UI has 3 buttons: Start Game, Controls, and Quit Game.

The Controls UI has the game’s controls + a button to return to the Main
Menu.

**Note:** See steps 4 and 5 in [What we Found Out](#what-we-found-out)
to create/add/remove a menu.

Access a level’s blueprint from the Blueprints menu:

1.  In the Menu level blueprint, open the Main Menu when the level is
    loaded.

2.  In the Main Menu blueprint, map the buttons to open the Controls
    Menu, Main game level, or quit the game on click.

**Pause Menu:**

Files required:

1.  UI (PauseMenu)

The Pause Menu has 2 buttons: Resume Game, and Quit to Main Menu.

In the Player controller, map the pause action to open the Pause Menu +
pause the game.

See step 7 in [What we Found Out](#what-we-found-out) to pause the game.

Map the buttons to open the Main Menu level, and to un-pause the game
and remove the pause menu from the viewport.

**HUD:**

Files Required:

1.  UI File (HUD)

The HUD has a single text element to indicate the player’s score. I put
in the top left corner and colored it yellow. Turn it into a variable by
checking the box at the top of the Designer’s details panel. This will
allow you to set the text every tick to display the Player’s current
collectible score.

What we found out
-----------------

1.  It is better to use a PlayerStart component rather than spawning the
    player object directly, as it makes it easier to reset/load levels.

2.  Anchor elements boxes to the center of the screen to allow them to
    resize gracefully.

3.  To open a level, or quit the game:

    a.  use Open Level or Quit Game in BP.

4.  How to open a menu:

    a.  To create/attach a UI view (also known as a widget) to an object
        (normally the player controller) in BP: Get Player Controller
        -&gt; Cast to \[Your Custom Class\] -&gt; Create Widget -&gt;
        Add to viewport.

    b.  Use to Set Input Mode to make the mouse behave correctly after
        the switch.

    c.  Use the Player Controller’s Set Show Mouse Cursor to set whether
        the cursor is visible or not.

5.  How to close a menu/open a new menu from within another menu:

    a.  First, remove the first menu from the Player Controller using
        Remove from Parent, then create a new menu as shown in step 4.

    b.  Remember to Set Input Mode appropriately.

6.  How to use Level Blueprints:

    a.  Access the Level Blueprints through the blueprints menu. This
        will allow you to run code when the level is loaded, such as
        creating a menu, or HUD on loading in.

7.  To pause the game:

    a.  Get Player Controller -&gt; Cast to \[Your Custom Class\] -&gt;
        Set Game Paused

\[Optional\] Open Issues/risks
------------------------------

 \[Optional\] Recommendations
-----------------------------
